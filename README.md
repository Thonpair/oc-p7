# OCP7

Pré-requis
* MySQL
* NodeJS

L'application a été développé avec NodeJS / Express ainsi que MySQL pour la partie back-end et VueJS 2 pour la partie front-end

## Description des dossiers

* back : dossier contenant la partie back-end du projet
* front : dossier contenant la partie front-end du projet

## Installation et paramétrage

Les fichiers README.md situés dans les dossiers back et front contiennent les différentes instructions d'installation pour les deux parties du projet


