# OCP7

L'application a été développé avec VueJS 2.

## FRONT-END

Le projet se situe dans le sous-dossier front. Lancer d'abord la commande suivante pour se déplacer dans le dossier : 

> cd front

### Installation

Lancer la commande pour lancer l'installation : 

> npm install

### Paramétrage (optionel)

Editer et adpater si besoin le fichier .env dans le répertoire principal du projet.


### Démarrage

Lancer la commande suivante pour lancer le service :

> npm run serve

Le service est accessible dans un navigateur à l'adresse suivante : http://127.0.0.1:8080

