import Vue from 'vue'
import Vuex from 'vuex'

Vue.config.productionTip = false

Vue.use(Vuex)


new Vue({
  render: h => h(require('./App').default),
}).$mount('#app')
