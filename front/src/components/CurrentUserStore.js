import Vuex from 'vuex'

const state = {
    server: {
        url: String,
    },
    user: {
        token: String,
        username: String,
        userid: Number,
        isAdmin: false,
    },
    authForm: {
        display: false,
        showLogin: true
    },
    commentForm: {
        articleId: -1
    },
    articleForm:{
        display: false,
        articleId: null
    }

}

const mutations =  {
    SETSERVERURL: (state, url) => {
        state.server.url = url;
    },
    AUTHENTIFICATION: (state, token) => {
        state.user.token = token;
        localStorage.setItem("token", token);
    },
    SETUSERNAME:(state, username) => {
        state.user.username = username;
        localStorage.setItem("username", username);
    },
    SETUSERID:(state, userid) => {
        state.user.userid = userid
    },
    SETUSERROLE:(state, role) => {
        state.user.isAdmin = role === "admin"
    },
    DISCONNECT:(state) => {
        state.user.token = String;
        state.user.username = String;
        state.user.userid = Number;
        localStorage.clear();
    },
    DISPLAYAUTHFORM: (state) => {
        state.authForm.display = true
    },
    HIDEAUTHFORM: (state) => {
        state.authForm.display = false
    },
    SHOWLOGIN: (state) => {
        state.authForm.showLogin = true
    },
    SHOWSUBSCRIBE: (state) =>{
        state.authForm.showLogin = false
    },
    SHOWCOMMENTS: (state, articleId) => {
        state.commentForm.articleId = articleId
    },
    HIDECOMMENTS: (state) => {
        state.commentForm.articleId = -1
    },
    SHOWARTICLEFORM: (state, articleId) => {
        state.articleForm.display = true;
        state.articleForm.articleId = articleId;
    },
    HIDEARTICLEFORM: (state) => {
        state.articleForm.display = false;
        state.articleForm.articleId = null;
    }
}

const getters = {
    serverUrl: state => state.server.url,
    token: state => state.user.token,
    headers_api: ((state) => { return {headers: { Authorization: "Bearer " + state.user.token, }}}),
    username: state => state.user.username,
    userid: state => state.user.userid,
    isAdmin: state => state.user.isAdmin,
    isConnected: state => state.user.token !== String,
    authFormDisplayed: state => state.authForm.display,
    showLogin: state => state.authForm.showLogin,
    commentsDisplayed: state => state.commentForm.articleId !== -1,
    commentsArticleId: state => state.commentForm.articleId,
    articleDisplayed: state => state.articleForm.display,
    updateArticleId: state => state.articleForm.articleId,
    newArticle: state => state.articleForm.articleId === null,
}

const actions = {
    setServerUrl: (store, url) => {
        store.commit(`SETSERVERURL`, url)
    },
    authentification: (store,token) => {
        store.commit('AUTHENTIFICATION', token)
    },
    setUsername: (store, username) => {
        store.commit('SETUSERNAME', username)
    },
    setUserrole: (store, role) => {
        store.commit('SETUSERROLE', role)
    },
    setUserid: (store, userid) => {
        store.commit('SETUSERID', userid)
    },
    disconnect: (store) => {
        store.commit('DISCONNECT')
    },
    hideauthform: (store) => {
        store.commit('HIDEAUTHFORM')
    },
    showLoginForm: (store) => {
        store.commit('SHOWLOGIN')
        store.commit('DISPLAYAUTHFORM')
    },
    showSubscribeForm: (store) => {
        store.commit('SHOWSUBSCRIBE')
        store.commit('DISPLAYAUTHFORM')
    },
    showCommentsForm: (store, articleId) => {
        store.commit('SHOWCOMMENTS', articleId)
    },
    hideCommentsForm: (store) => {
        store.commit('HIDECOMMENTS')
    },
    showArticleForm: (store, articleid) => {
        store.commit('SHOWARTICLEFORM', isNaN(articleid) ? null : articleid)
    },
    hideArticleForm: (store) => {
        store.commit('HIDEARTICLEFORM')
    }

}

let store = new Vuex.Store({
    state: state,
    mutations:mutations,
    getters: getters,
    actions: actions
})

global.store = store

export default store