# OCP7

Pré-requis
* MySQL
* NodeJS

## MySQL

La base de données utilisée pour ce projet est MySQL.

### Création de la base de données

Le fichier SQL à importer est **app/db/create-db.sql** et contient les requêtes pour la création de la base de données et la création d'un utilisateur mysql **ocp7**. Le fichier peut être importé via phpmyadmin ou en ligne de commandes.

#### Import via phpMyAdmin

Dans l'interface prinicpale de phpMyAdmin, en étant connecté avec un utilisateur ayant les droits suffisants : 
* ouvrir le menu **Importer**
* dans la zone **Fichier à importer**, cliquer sur le bouton **Parcourir** et sélectionner le fichier **app/db/create-db.sql**
* cliquer sur le bouton Exécuter

#### Import en ligne de commande

En tant qu'administrateur ou root, lancer la commande suivante :
> mysql -u root < app/db/create-db.sql

## BACK-END

### Installation

Se placer dans le répertoire principal du projet puis lancer la commande : 

> npm install

### Paramétrage (optionel)

Editer et adpater si besoin le fichier .env dans le répertoire principal du projet

### Test (optionel)

Des tests unitaires ont été mis en place pour vérifier le bon fonctionnement du projet. Les tests unitaires créée également un article d'exemple et créé des utilisateurs d'exemple. Pour lancer les tests, lancer la commande : 

> npm run test

Les noms d'utilisateurs et leurs mots de passe sont affichés à la fin du test.

### Démarrage

Le projet peut être démarré via la commande :

> npm run start

Le service est accessible via http://127.0.0.1:3000/api


