const { body } = require('express-validator');
const Role = require('../../utils/userRoles.utils');

exports.createUserSchema = [
    body('username')
        .exists()
        .withMessage("Le nom d'utilisateur n'est pas renseigné.")
        .isLength({ min: 3 })
        .withMessage("Le nom d'utilisateur doit faire au moins 3 caractères.")
        .isLength({ max: 25})
        .withMessage("Le nom d'utilisateur doit faire moins de 25 caractères."),
    body('email')
        .exists()
        .withMessage("L'adresse e-mail n'est pas renseignée.")
        .isEmail()
        .withMessage("L'adresse e-mail est invalide.")
        .normalizeEmail(),
    body('role')
        .optional()
        .isIn([Role.Admin, Role.User])
        .withMessage('Invalid Role type'),
    body('password')
        .exists()
        .withMessage("Le mot de passe n'est pas renseigné.")
        .notEmpty()
        .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[_\W]).{8,}$/)
        .withMessage("Le mot de passe doit avoir au moins 1 lettre majuscule, 1 lettre minuscule, 1 chiffre, 1 caractère spécial, longueur minimum 8 caractères"),
    body('confirm_password')
        .exists()
        .withMessage("La confirmation de mot de passe n'est pas renseignée.")
        .custom((value, { req }) => value === req.body.password)
        .withMessage("La confirmation de mot de passe ne correspond pas au premier mot de passe.")
];

exports.validateLogin = [
    body('email')
        .exists()
        .withMessage("L'adresse e-mail n'est pas renseignée.")
        .isEmail()
        .withMessage("L'adresse e-mail est invalide.")
        .normalizeEmail(),
    body('password')
        .exists()
        .withMessage("Le mot de passe n'est pas renseigné.")
        .notEmpty()
        .withMessage("Le mot de passe ne peut pas être vide.")
];

