const { body } = require('express-validator');

exports.createLikeSchema = [
    body("articleid")
    .exists()
    .withMessage('article id is missing')
]