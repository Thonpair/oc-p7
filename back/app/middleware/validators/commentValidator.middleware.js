const { body } = require('express-validator');

exports.createCommentSchema = [
    body('comment')
        .exists()
        .withMessage('Comment is required')
        .isLength({ min: 1 })
        .withMessage('Must be at least 1 chars long')
        .isLength({ max: 65535 })
        .withMessage('Must be at most 65 535 chars long')
];

exports.updateCommentSchema = [
    body('content')
        .exists()
        .withMessage('Comment is required')
        .isLength({ min: 1 })
        .withMessage('Must be at least 1 chars long')
        .isLength({ max: 65535 })
        .withMessage('Must be at most 65 535 chars long')
];