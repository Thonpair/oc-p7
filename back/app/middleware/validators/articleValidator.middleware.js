const { body } = require('express-validator');

exports.createArticleSchema = [
    body('title')
        .exists()
        .withMessage('Title is required')
        .isLength({ min: 1 })
        .withMessage('Must be at least 1 chars long')
        .isLength({ max: 255 })
        .withMessage('Must be at most 255 chars long')
];

exports.updateArticleSchema = [
    body('title')
        .exists()
        .withMessage('Title is required')
        .isLength({ min: 1 })
        .withMessage('Must be at least 1 chars long')
        .isLength({ max: 255 })
        .withMessage('Must be at most 255 chars long')
]
