require('dotenv');
/** gestion des erreurs, retourne une erreur 500 en cas de problème + message */
function errorMiddleware(error, req, res, next) {
    let { status = 500, message, data } = error;

    if (process.env.NODE_ENV === 'TEST'){
        console.log(`[Error] ${error}`);
    }

    message = status === 500 || !message ? 'Internal server error' : message;

    error = {
        type: 'error',
        status,
        message,
        ...(data) && data
    }

    res.status(status).send(error);
}

module.exports = errorMiddleware;
