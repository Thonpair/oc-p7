DROP DATABASE IF EXISTS ocp7;   
CREATE DATABASE IF NOT EXISTS ocp7;   


DROP USER IF EXISTS 'ocp7';

CREATE USER 'ocp7' IDENTIFIED BY 'ocp7';
ALTER USER 'ocp7' REQUIRE NONE;
GRANT ALL PRIVILEGES ON `ocp7`.* TO 'ocp7'; 

USE ocp7;

DROP TABLE IF EXISTS user; 
DROP TABLE IF EXISTS article; 
DROP TABLE IF EXISTS comment;

CREATE TABLE IF NOT EXISTS user ( 
    id          INT PRIMARY KEY auto_increment, 
    username    VARCHAR(25) UNIQUE NOT NULL, 
    password    CHAR(60) NOT NULL, 
    email       VARCHAR(100) UNIQUE NOT NULL, 
    role        ENUM('admin', 'user') DEFAULT 'user'
  ); 

  
CREATE TABLE IF NOT EXISTS article 
  ( 
    id          INT PRIMARY KEY auto_increment, 
    title       VARCHAR(255) NOT NULL, 
    imageUrl    VARCHAR(255) NOT NULL,
    userid      INT NOT NULL
  ); 

  CREATE TABLE IF NOT EXISTS  comment
  ( 
    id          INT PRIMARY KEY auto_increment, 
    content     TEXT NOT NULL,
    userid      INT NOT NULL,
    articleid   INT NOT NULL
  ); 

  CREATE TABLE IF NOT EXISTS articlelike
  (
    articleid INT NOT NULL,
    userid INT NOT NULL,
    PRIMARY KEY(articleid, userid)
  )