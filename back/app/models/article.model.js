const query = require('../db/db-connection');
const { multipleColumnSet } = require('../utils/common.utils');
const Role = require('../utils/userRoles.utils');
const TableName = require('../utils/tableNames.utils');

/** Gestion des articles */
class ArticleModel {
    tableName = TableName.article;

    /** recherche un article selon plusieurs critèques  */
    find = async (params = {}) => {
        /** Retourne les articles du plus récent au plus ancien */
        const sqlOrder = "ORDER BY id DESC";
        let sql = `SELECT * FROM ${this.tableName}`;
        if (!Object.keys(params).length) {
            sql += ` ${sqlOrder}`;
            return await query(sql);
        }
        const { columnSet, values } = multipleColumnSet(params)
        sql += ` WHERE ${columnSet} ${sqlOrder}`;
        let result = await query(sql, [...values]);
        return result
    }

    /** Retourne un article selon l'id */
    findOne = async (id) => {
        let sql = `SELECT * FROM ${this.tableName} WHERE id = ?`;
        return await query(sql, [id]);
    }

    /** Création d'un article */
    create = async ({ title, imageUrl, userId }) => {
        const sql = `INSERT INTO ${this.tableName}
        (title, imageUrl, userid) VALUES (?,?,?)`;
        const result = await query(sql, [title, imageUrl, userId]);
        const affectedRows = result ? result.affectedRows : 0;
        return affectedRows;
    }

    /** Met à jour un article, soit par son auteur, soit par un admin */
    update = async (params, id, userId, role = Role.User) => {
        const { columnSet, values } = multipleColumnSet(params)
        let sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;
        if (role === Role.Admin) {
            const result = await query(sql, [...values, id]);
            const affectedRows = result ? result.affectedRows : 0;
            return affectedRows;
        }
        sql += ` AND userid = ?`
        const result = await query(sql, [...values, id, userId]);
        const affectedRows = result ? result.affectedRows : 0;
        return affectedRows;
    }

    /** Supprime un article, soit par son auteur, soit par un admin */
    delete = async (id, user, role) => {
        let sql = `DELETE FROM ${this.tableName} WHERE id = ${id}`;
        if (role !== Role.Admin) {
            sql += ` AND userid = ${user}`;
        }
        const result = await query(sql);
        const affectedRows = result ? result.affectedRows : 0;
        return affectedRows;
    }
}

module.exports = new ArticleModel;