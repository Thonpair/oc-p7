const query = require('../db/db-connection');
const { multipleColumnSet } = require('../utils/common.utils');
const Role = require('../utils/userRoles.utils');
const TableName = require('../utils/tableNames.utils');

/** Gestion des utilisateurs */
class UserModel {
    tableName = TableName.user;
    /**Recherche un utilisateur */
    findOne = async (params) => {
        const { columnSet, values } = multipleColumnSet(params)
        const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet} LIMIT 1`;
        
        const result = await query(sql, [...values]);

        // return back the first row (user)
        return result[0];
    }

    /** Retourne les informations d'un utilisateur */
    findOneInfos = async (params) => {
        const { columnSet, values } = multipleColumnSet(params)
        const sql = `SELECT id, username, role FROM ${this.tableName}
        WHERE ${columnSet} LIMIT 1`;
        
        const result = await query(sql, [...values]);

        // return back the first row (user)
        return result[0];
    }

    /** Créé un nouvel utilisateur */
    create = async ({ username, password, email, role = Role.User }) => {
        const sql = `INSERT INTO ${this.tableName}
        (username, password, email, role) VALUES (?,?,?,?)`;

        const result = await query(sql, [username, password, email, role]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }
}

module.exports = new UserModel;
