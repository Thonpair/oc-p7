const { async } = require('regenerator-runtime');
const query = require('../db/db-connection');
const TableName = require('../utils/tableNames.utils');

/** gestion des likes sur les articles*/
class likeModel {
    /** créé un like associé à un article pour un utilisateur */
    create = async (articleid, userid) => {
        const sql = `INSERT INTO ${TableName.like} (articleid, userid)
        SELECT ?, ?
        WHERE (select count(*) from ${TableName.article} where id=?) > 0
        AND (select count(*) from ${TableName.user} where id=?) > 0;`;
        const result = await query(sql, [articleid, userid, articleid, userid]);
        const affectedRows = result ? result.affectedRows : 0;
        return affectedRows;
    }

    /** retourne le nombre total de like associé à un article si l'id utilisateur n'est pas renseigné
     * ou si l'utilisateur a aimé un article si l'id utilisateur a été renseigné */
    count = async(articleid, userid) => {
        let sql = `SELECT COUNT(*) AS score from ${TableName.like} where articleid =  ? `
        if (!userid){
            const result = await query(sql, [articleid])
            return result[0];    
        }
        sql += 'AND userid = ?'
        const result = await query(sql, [articleid, userid])
        return {"userlike" : result[0].score === 1};
    }

    /** supprime un like*/
    deleteOne = async(articleid, userid) => {
        const sql = `DELETE FROM ${TableName.like} WHERE articleid = ${articleid} AND userid = ${userid}`;
        const result = await query(sql, [articleid, userid]);
        const affectedRows = result ? result.affectedRows : 0;
        return affectedRows;
    }

    /** supprime tous les likes associés à un article */
    deleteAll = async (articleId) => {
        let sql = `DELETE FROM ${TableName.like}
        WHERE articleid = ?`;
        return await query(sql, [articleId]);
    }
}

module.exports = new likeModel;
