const query = require('../db/db-connection');
const { multipleColumnSet } = require('../utils/common.utils');
const Role = require('../utils/userRoles.utils');
const TableName = require('../utils/tableNames.utils');

/** Gestion des commentaires */
class CommentModel {
    commentTableName = TableName.comment;
    articleTableName = TableName.article;

    /** retourne les commentaires associés à un article via l'id de l'article */    
    find = async (articleid) => {
        let sql = `SELECT * FROM ${this.commentTableName} WHERE articleid= ?`;
        return await query(sql, [articleid]);
    }

    /** retourne le nombre de commentaires associés à un article*/
    count = async (articleid) => {
        let sql = `SELECT COUNT(*) AS countComment FROM ${this.commentTableName} WHERE articleid= ?`;
        const result = await query(sql, [articleid]);
        return result[0];
    }

    /** créé un commentaire pour un utilisateur sur un article, identifiés par leurs id*/
    create = async (content, userid, articleid) => {
        const sql = `INSERT INTO comment (content, userid, articleid)
        SELECT ?, ?, ?
        WHERE (select count(*) from article where id=?) > 0;`;
        const result = await query(sql, [content, userid, articleid, articleid]);
        const affectedRows = result ? result.affectedRows : 0;
        return affectedRows;
    }

    /** Met à jour un commentaire, seulement pour l'auteur ou un administrateur */
    update = async (params, commentid, userId, role = Role.User) => {
        const { columnSet, values } = multipleColumnSet(params)
        let sql = `UPDATE ${this.commentTableName} SET ${columnSet} WHERE id = ?`;
        if (role === Role.Admin) {
            const result = await query(sql, [...values, commentid]);
            const affectedRows = result ? result.affectedRows : 0;
            return affectedRows;
        }
        sql += ` AND userid = ?`
        const result = await query(sql, [...values, commentid, userId]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }

    /** supprime un commentaire, seulement pour l'auteur ou un administrateur */
    delete = async (commentid, userId, role = Role.User) => {
        let sql = `DELETE FROM ${this.commentTableName}
        WHERE id = ?`;
        if (role === Role.Admin) {
            const result = await query(sql, [commentid]);
            const affectedRows = result ? result.affectedRows : 0;
            return affectedRows;
        }
        sql += ` AND userid = ?`;
        const result = await query(sql, [commentid, userId]);
        const affectedRows = result ? result.affectedRows : 0;
        return affectedRows;
    }

    /** supprime tous les commentaires associés à un article */
    deleteFromArticle = async (articleId) => {
        let sql = `DELETE FROM ${this.commentTableName}
        WHERE articleid = ?`;
        return await query(sql, [articleId]);
    }
}

module.exports = new CommentModel;
