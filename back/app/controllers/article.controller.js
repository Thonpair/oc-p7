const articleModel = require("../models/article.model");
const commentModel = require("../models/comment.model");
const likeModel = require('../models/like.model');
const HttpException = require('../utils/HttpException.utils');
const { externalFilePath } = require('../utils/fileUrl.utils');
const { checkValidationAndFile, checkValidation } = require('../utils/validation.utils');

class ArticleController {
    /** Création d'un article */
    createArticle = async (req, res, next) => {
        /** Vérification du contenu envoyé + du fichier */
        checkValidationAndFile(req);
        /** Génération de l'url pour l'image */
        req.body.imageUrl = externalFilePath(req.file.filename);
        /** Récupération de l'id de l'utilisateur */
        req.body.userId = req.currentUser.id;
        /** Récupération du résultat de la requête de création d'article */
        const result = await articleModel.create(req.body);
        /** Rejette en cas d'erreur SQL */
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        /** Retourne un message si l'article a bien été créé */
        res.status(201).send("L'article a été créé");

        
    };
    /** Retourne l'ensemble des article */
    getArticles = async (req, res, next) => {
        const result = await articleModel.find(req.query);
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send(result);
    }
    /** Retourne un article selon l'id fourni */
    getOneArticle = async (req, res, next) => {
        const result = await articleModel.findOne(req.params.id);
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send(result);
    }

    /** Met à jour l'article */
    updateArticle = async (req, res, next) => {
        checkValidation(req);
        /** si une image a été fourni, préparation des paramètres pour fournir la nouvelle url */
        if (req.file){
            req.body.imageUrl = externalFilePath(req.file.filename);
        }
        /** Récupère l'id et le role de l'utilisateur */
        const userId = req.currentUser.id;
        const userRole = req.currentUser.role;
        const articleId = req.params.id;
        /** Nettoyage du body */
        delete req.body.id;
        const result = await articleModel.update(req.body, articleId, userId, userRole)
        /** Le nombre d'article modifié est à 0 : problème de droits d'accès */
        if (result < 1){
            throw new HttpException(401, 'Access denied. Missing right');
        }
        /** Erreur d'execution SQL */
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send("L'article a été modifié");
    }

    deleteArticle = async (req, res, next) => {
        /** Récupère l'id et le role de l'utilisateur */
        const userId = req.currentUser.id;
        const userRole = req.currentUser.role;
        const articleId = req.params.id;
        const result = await articleModel.delete(articleId, userId, userRole)
        if (result < 1){
            throw new HttpException(401, 'Access denied. Missing right');
        }
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        /** Si l'article peut bien être supprimé, on supprime les commentaires associés */
        const resultComment = await commentModel.deleteFromArticle(articleId)
        if (!resultComment) {
            throw new HttpException(500, 'Something went wrong');
        }
        /** On supprime également les like associés */
        const resultLike = await likeModel.deleteAll(articleId)
        if(!resultLike){
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send("L'article a été supprimé");
    }
}

module.exports = new ArticleController;