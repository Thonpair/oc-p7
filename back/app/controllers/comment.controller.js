const commentModel = require("../models/comment.model");
const HttpException = require('../utils/HttpException.utils');
const { checkValidation } = require('../utils/validation.utils')

/** Gestion des commentaires */
class commentController {
    /** Création d'un commentaire */
    createComment = async (req, res, next) => {
        checkValidation(req);
        const result = await commentModel.create(req.body.comment, req.currentUser.id, req.params.articleid);
        if (!result) {
            throw new HttpException(404, 'Article not found');
        }
        res.status(201).send('Comment was created!');
    }
    /** Récupération de la liste des commentaires d'un article */
    getComments = async (req, res, next) => {
        checkValidation(req);
        const result = await commentModel.find(req.params.articleid);
        res.status(201).send(result)
    }
    /** Récupération du nombre de commentaires sur un article */
    getCountComments = async (req, res, next) => {
        const result = await commentModel.count(req.params.articleid);
        res.status(201).send(result)
    }
    /** Met à jour un commentaire */
    updateComment = async (req, res, next) => {
        checkValidation(req);
        const userId = req.currentUser.id;
        const userRole = req.currentUser.role;
        const commentId = req.params.commentid;
        delete req.body.id;
        const result = await commentModel.update(req.body, commentId, userId, userRole)
        if (result < 1){
            throw new HttpException(401, 'Access denied. Missing right');
        }
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send('Comment was updated!');
    }

    /** Supprime un commentaire */
    deleteComment = async (req, res, next) => {
        checkValidation(req)
        const userId = req.currentUser.id;
        const userRole = req.currentUser.role;
        const commentId = req.params.commentid;
        const result = await commentModel.delete(commentId, userId, userRole)
        if (result < 1){
            throw new HttpException(401, 'Access denied. Missing right');
        }
        if(!result){
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send('Comment was deleted!')
    }
}

module.exports = new commentController;