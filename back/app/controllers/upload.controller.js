/** Import des dépendances */
const path = require("path")
/** Récupération du fichier de configuration */
require('dotenv').config();

/** Retourne un fichier dans le dossier upload, avec l'id fourni en paramètre */
exports.get = (req, res, next) => {
    res.sendFile(path.join(__dirname, '..', '..', process.env.UPLOAD_DIR, req.params.id))
}