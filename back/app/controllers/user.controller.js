const UserModel = require('../models/user.model');
const HttpException = require('../utils/HttpException.utils');
const { checkValidation } = require('../utils/validation.utils')

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

/** Gestion des utilisateurs */
class UserController {
    /** Création d'un utilisateur */
    createUser = async (req, res, next) => {
        /** Vérification des paramètrs fourni */
        checkValidation(req);
        /** chiffrement du mot de passe */
        await this.hashPassword(req);
        /** Lancement de la création de l'utilisateur */
        const result = await UserModel.create(req.body);
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send('User was created!');
    };


    /** Authentification d'un utilisateur */
    userLogin = async (req, res, next) => {
        /** Vérification des paramètrs fourni */
        checkValidation(req);
        /** Récupération des éléments de la requête */
        const { email, password: pass } = req.body;
        /** Recherche de l'utilisateur avec son adresse email */
        const user = await UserModel.findOne({ email });
        if (!user) {
            throw new HttpException(401, 'Adresse e-mail introuvable');
        }
        /** Compare le mot de passe  */
        const isMatch = await bcrypt.compare(pass, user.password);
        if (!isMatch) {
            throw new HttpException(401, 'Mot de passe incorrect');
        }
        /** Création du token */
        const secretKey = process.env.SECRET_JWT || "";
        const token = jwt.sign({ user_id: user.id.toString() }, secretKey, {
            expiresIn: '24h'
        });
        /** Envoi du nom d'utilisateur et du token */
        const { password, ...userWithoutPassword } = user;
        res.send({ ...userWithoutPassword, token });
    };

    /** retourne id, nom d'utilisateur et role */
    infos = async (req, res, next) => {
        checkValidation(req);
        const userId = req.params.userid ? req.params.userid : req.currentUser.id;
        const result = await UserModel.findOneInfos({id: userId});
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send(result);
    }

    // hash du mot de passe s'il existe
    hashPassword = async (req) => {
        if (req.body.password) {
            req.body.password = await bcrypt.hash(req.body.password, 8);
        }
    }
}

module.exports = new UserController;
