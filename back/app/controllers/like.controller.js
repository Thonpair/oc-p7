const likeModel = require("../models/like.model");
const HttpException = require('../utils/HttpException.utils');
const { checkValidation } = require('../utils/validation.utils')

/** Gestion des likes */
class likeController {
    /** Ajour un like sur un article, avec l'id de l'article et l'id de l'utilisateur */
    createLike = async (req, res, next) => {
        checkValidation(req);
        const result = await likeModel.create(req.body.articleid, req.currentUser.id);
        if (!result) {
            throw new HttpException(404, 'Article not found');
        }
        res.status(201).send('Like was added!');
    }

    /** Compte le nombre de like sur un article */
    countArticleLikes = async(req, res, next) => {
        const result = await likeModel.count(req.params.articleid);
        res.status(201).send(result)
    }

    /** Retourne si l'utilisateur a aimé ou non un article, en fournissant leurs id */
    userLike = async(req, res, next) => {
        const result = await likeModel.count(req.params.articleid, req.currentUser.id);
        res.status(201).send(result)
    }

    /** Supprime un like d'un article */
    deleteLike = async(req, res, next) => {
        const result = await likeModel.deleteOne(req.params.articleid, req.currentUser.id)
        if (result < 1){
            throw new HttpException(404, 'Like not found');
        }
        if(!result){
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).send('like was deleted!')
    }

}

module.exports = new likeController;
