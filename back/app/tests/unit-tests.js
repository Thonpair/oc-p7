const chaiHttp = require('chai-http');
const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const fs = require('fs');
chai.use(chaiHttp);
const server = require('../server');

const date = Date.now().toString().substring(7)

const firstAccount = {
    username: `user1-${date}`,
    email: `firstemail${date}@example.com`,
    password: `P4$$w0rd`,
    role: 'user',
    id: '',
    token: '',
    articleId: 0,
    articleId2: 0,
    commentId: 0,
    commentId2: 0
}

const secondAccount = {
    username: `user2-${date}`,
    email: `secondemail${date}@example.com`,
    password: `P4$$w0rd`,
    role: 'admin',
    id: '',
    token: '',
    articleId: 0
}

const thirdAccount = {
    username: `user3-${date}`,
    email: `thirdemail${date}@example.com`,
    password: `P4$$w0rd`,
    role: 'user',
    id: '',
    token: '',
    articleId: 0,
    articleId2: 0,
    articleId3: 0,
    commentId: 0,
    commentId2: 0,
    commentId3: 0
}

const badValues = {
    usernameEmpty: '',
    usernameTooShort: 'ab',
    usernameTooLong: 'SorryThisUsernameIsTooLong',
    passwordEmpty: '',
    passwordShort: 'a12$A',
    passwordIncorrect: 'Bad_Password',
    passwordWithoutNum: 'Pa$$word',
    passwordWithoutSpec: 'P4SSword',
    passwordWithoutLetter: '74$$30!!',
    passwordWithoutLetterMin: 'P4$$W0RD',
    passwordWithoutLetterMaj: 'p4$$w0rd',
    invalidEmailWithoutAt: 'totoAtexample.com',
    invalidEmailWithoutExtension: 'toto@example',
    invalidEmailWithoutDomain: 'toto@',
    invalidEmailWithoutUsername: '@example.com',
    invalidRole: 'SuperUser'
}

const firstCategorie = {
    id: 0,
    name: `Categorie 1 ${date}`
}
const secondCategorie = {
    id: 0,
    name: `Categorie 2 ${date}`
}
const thirdCategorie = {
    id: 0,
    name: `Categorie 3 ${date}`
}



suite('Tests fonctionnels', () => {
    suite('Utilisateur', () => {
        suite('Création', () => {
            test('Création nouvel utilisateur', (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": firstAccount.username,
                        "email": firstAccount.email,
                        "password": firstAccount.password,
                        "confirm_password": firstAccount.password,
                        "role": firstAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'User was created!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
            test('Création une seconde fois de ce nouvel utilisateur', (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": firstAccount.username,
                        "email": firstAccount.email,
                        "password": firstAccount.password,
                        "confirm_password": firstAccount.password,
                        "role": firstAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.status, 409);
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, `Duplicate entry '${firstAccount.username}' for key 'user.username'`);
                        done()
                    })
            })
            test('Création second utilisateur, en administrateur', (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": secondAccount.username,
                        "email": secondAccount.email,
                        "password": secondAccount.password,
                        "confirm_password": secondAccount.password,
                        "role": secondAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'User was created!');
                        assert.equal(res.status, 201);
                        done()
                    })
            })

            test("Création nouvel utilisateur sans nom d'utilisateur", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "confirm_password": thirdAccount.password,
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'username');
                        assert.equal(res.body.errors[0].msg, "Le nom d'utilisateur n'est pas renseigné.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test("Création nouvel utilisateur sans adresse email", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": thirdAccount.username,
                        "password": thirdAccount.password,
                        "confirm_password": thirdAccount.password,
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'email');
                        assert.equal(res.body.errors[0].msg, "L'adresse e-mail n'est pas renseignée.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test("Création nouvel utilisateur sans mot de passe", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": thirdAccount.username,
                        "email": thirdAccount.email,
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'password');
                        assert.equal(res.body.errors[0].msg, "Le mot de passe n'est pas renseigné.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })

            test("Création nouvel utilisateur avec confirmation de mot de passe invalide", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": thirdAccount.username,
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "confirm_password": 'invalid password',
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'confirm_password');
                        assert.equal(res.body.errors[0].msg, "La confirmation de mot de passe ne correspond pas au premier mot de passe.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test("Création nouvel utilisateur sans confirmation de mot de passe", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": thirdAccount.username,
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'confirm_password');
                        assert.equal(res.body.errors[0].msg, "La confirmation de mot de passe n'est pas renseignée.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test("Création nouvel utilisateur avec le nom d'utilisateur vide", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": badValues.usernameEmpty,
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "confirm_password": thirdAccount.password,
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'username');
                        assert.equal(res.body.errors[0].msg, "Le nom d'utilisateur doit faire au moins 3 caractères.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test("Création nouvel utilisateur avec le nom d'utilisateur trop court (3 caractères)", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": badValues.usernameTooShort,
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "confirm_password": thirdAccount.password,
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'username');
                        assert.equal(res.body.errors[0].msg, "Le nom d'utilisateur doit faire au moins 3 caractères.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test("Création nouvel utilisateur avec le nom d'utilisateur trop long", (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": badValues.usernameTooLong,
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "confirm_password": thirdAccount.password,
                        "role": thirdAccount.role
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'username');
                        assert.equal(res.body.errors[0].msg, "Le nom d'utilisateur doit faire moins de 25 caractères.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test('Création troisième utilisateur avec rôle invalide', (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": thirdAccount.username,
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "confirm_password": thirdAccount.password,
                        "role": badValues.invalidRole
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'role');
                        assert.equal(res.body.errors[0].msg, 'Invalid Role type');
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test('Création troisième utilisateur sans rôle', (done) => {
                chai.request(server)
                    .post('/api/users')
                    .send({
                        "username": thirdAccount.username,
                        "email": thirdAccount.email,
                        "password": thirdAccount.password,
                        "confirm_password": thirdAccount.password
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'User was created!');
                        assert.equal(res.status, 201);
                        done()
                    })
            })
        })
        suite('Authentification', () => {
            test('Connexion sans adresse e-mail', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        password: firstAccount.password
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'email');
                        assert.equal(res.body.errors[0].msg, "L'adresse e-mail n'est pas renseignée.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test('Connexion avec une adresse e-mail invalide', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        email: badValues.invalidEmailWithoutDomain,
                        password: firstAccount.password
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'email');
                        assert.equal(res.body.errors[0].msg, "L'adresse e-mail est invalide.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test('Connexion sans mot de passe', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        email: firstAccount.email
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'password');
                        assert.equal(res.body.errors[0].msg, "Le mot de passe n'est pas renseigné.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test('Connexion avec mot de passe vide', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        email: firstAccount.email,
                        password: ''
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'password');
                        assert.equal(res.body.errors[0].msg, "Le mot de passe ne peut pas être vide.");
                        assert.equal(res.body.errors[0].location, 'body');
                        done()
                    })
            })
            test('Connexion échouée avec mot de passe  incorrect', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        email: firstAccount.email,
                        password: badValues.passwordIncorrect
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Mot de passe incorrect');
                        assert.equal(res.body.status, 401);
                        done()
                    })
            })
            test('Connexion avec mot de passe correct 1/3', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        email: firstAccount.email,
                        password: firstAccount.password
                    })
                    .end((err, res) => {
                        assert.isNumber(res.body.id);
                        assert.isString(res.body.username);
                        assert.equal(res.body.email, firstAccount.email);
                        assert.equal(res.body.role, firstAccount.role);
                        assert.isString(res.body.token);
                        firstAccount.id = res.body.id;
                        firstAccount.token = res.body.token;
                        done()
                    })
            })
            test('Connexion avec mot de passe correct 2/3', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        email: secondAccount.email,
                        password: secondAccount.password
                    })
                    .end((err, res) => {
                        assert.isNumber(res.body.id);
                        assert.isString(res.body.username);
                        assert.equal(res.body.email, secondAccount.email);
                        assert.equal(res.body.role, secondAccount.role);
                        assert.isString(res.body.token);
                        secondAccount.id = res.body.id;
                        secondAccount.token = res.body.token;
                        done()
                    })
            })
            test('Connexion avec mot de passe correct 3/3', (done) => {
                chai.request(server)
                    .post('/api/users/login')
                    .send({
                        email: thirdAccount.email,
                        password: thirdAccount.password
                    })
                    .end((err, res) => {
                        assert.isNumber(res.body.id);
                        assert.isString(res.body.username);
                        assert.equal(res.body.email, thirdAccount.email);
                        assert.equal(res.body.role, thirdAccount.role);
                        assert.isString(res.body.token);
                        thirdAccount.id = res.body.id;
                        thirdAccount.token = res.body.token;
                        done()
                    })
            })
        })
        suite('Affichage', () => {
            test(`Affichage des informations d'un utilisateur connecté sans authentification`, (done) => {
                chai.request(server)
                    .get('/api/users/whoami')
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test(`Affichage des informations d'un utilisateur connecté`, (done) => {
                chai.request(server)
                    .get('/api/users/whoami')
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.body.username, firstAccount.username);
                        assert.equal(res.body.role, firstAccount.role);
                        done()
                    })
            })

            test(`Affichage des informations d'un autre utilisateur sans id`, (done) => {
                chai.request(server)
                    .get(`/api/users/whois/`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Endpoint Not Found');
                        assert.equal(res.body.status, 404);
                        done()
                    })
            })

            test(`Affichage des informations d'un autre utilisateur à partir de son id`, (done) => {
                chai.request(server)
                    .get(`/api/users/whois/${thirdAccount.id}`)
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.body.username, thirdAccount.username);
                        assert.equal(res.body.role, thirdAccount.role);
                        done()
                    })
            })
        })
    })
    suite('Article', () => {
        suite('Création', () => {
            test("Création d'un article sans authentification", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nom de l'article")
                    .field("userId", firstAccount.id)
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })

            test("Création d'un article sans titre", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'title');
                        assert.equal(res.body.errors[0].msg, 'Title is required');
                        assert.equal(res.body.errors[0].location, 'body');
                        done();
                    })
            })

            test("Création d'un article sans image", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .field("title", "Nom de l'article")
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].location, 'file');
                        assert.equal(res.body.errors[0].msg, 'File is required');
                        done();
                    })
            })

            test("Création d'un article 1/5", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nom de l'article 1")
                    .end((err, res) => {
                        assert.equal(res.text, 'L\'article a été créé')
                        assert.equal(res.status, 201);
                        done();
                    })
            })
            test("Création d'un article 2/5", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example2.jpg'), 'example2.jpg')
                    .field("title", "Nom de l'article 2")
                    .end((err, res) => {
                        assert.equal(res.text, 'L\'article a été créé')
                        assert.equal(res.status, 201);
                        done();
                    })
            })
            test("Création d'un article 3/5", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nom de l'article 3")
                    .end((err, res) => {
                        assert.equal(res.text, 'L\'article a été créé')
                        assert.equal(res.status, 201);
                        done();
                    })
            })
            test("Création d'un article 4/5", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nom de l'article 4")
                    .end((err, res) => {
                        assert.equal(res.text, 'L\'article a été créé')
                        assert.equal(res.status, 201);
                        done();
                    })
            })
            test("Création d'un article 5/5", (done) => {
                chai.request(server)
                    .post('/api/articles')
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nom de l'article 5")
                    .end((err, res) => {
                        assert.equal(res.text, 'L\'article a été créé')
                        assert.equal(res.status, 201);
                        done();
                    })
            })
        })
        suite('Affichage', () => {

            test("Affichage de la liste des articles", (done) => {
                chai.request(server)
                    .get('/api/articles')
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        const articles = res.body;
                        articles.map(article => {
                            assert.isNumber(article.id);
                            assert.isString(article.title);
                            assert.isString(article.imageUrl);
                            assert.isNumber(article.userid);
                        })
                        done();
                    })
            })

            test("Affichage des articles d'un utilisateur", (done) => {
                chai.request(server)
                    .get('/api/articles')
                    .query({ "userId": firstAccount.id })
                    .end((err, res) => {
                        firstAccount.articleId = res.body[0].id;
                        firstAccount.articleId2 = res.body[1].id;
                        assert.equal(res.status, 201);
                        assert.equal(res.body.length, 2);
                        done();
                    })
            })

            test("Affichage des articles d'un autre utilisateur", (done) => {
                chai.request(server)
                    .get('/api/articles')
                    .query({ "userId": thirdAccount.id })
                    .end((err, res) => {
                        thirdAccount.articleId = res.body[0].id;
                        thirdAccount.articleId2 = res.body[1].id;
                        thirdAccount.articleId3 = res.body[2].id;
                        assert.equal(res.status, 201);
                        assert.equal(res.body.length, 3);
                        done();
                    })
            })

            test("Affichage d'un article depuis son id", (done) => {
                chai.request(server)
                    .get(`/api/articles/${thirdAccount.articleId}`)
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.body.length, 1);
                        assert.equal(res.body[0].id, thirdAccount.articleId);
                        assert.isString(res.body[0].title);
                        assert.isString(res.body[0].imageUrl);
                        assert.isNumber(res.body[0].userid);
                        done();
                    })
            })
        })
        suite("Mise à jour", () => {
            test("Mise à jour d'un article sans identifiant", (done) => {
                chai.request(server)
                    .put('/api/articles/2')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nom de l'article")
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Mise à jour d'un article par un autre utilisateur non propriétaire", (done) => {
                chai.request(server)
                    .put(`/api/articles/${thirdAccount.articleId}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nouveau nom de l'article")
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. Missing right');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Mise à jour d'un article par un administrateur", (done) => {
                chai.request(server)
                    .put(`/api/articles/${firstAccount.articleId}`)
                    .set({ Authorization: `Bearer ${secondAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nouveau nom de l'article")
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'L\'article a été modifié')
                        done();
                    })
            })
            test("Mise à jour d'un article par le propriétaire", (done) => {
                chai.request(server)
                    .put(`/api/articles/${firstAccount.articleId}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .attach('image', fs.readFileSync('./example1.jpg'), 'example1.jpg')
                    .field("title", "Nouveau nouveau nom de l'article")
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'L\'article a été modifié')
                        done();
                    })
            })
        })
        suite("Suppression", () => {
            test("Suppression d'un article sans authentification", (done) => {
                chai.request(server)
                    .delete(`/api/articles/${firstAccount.articleId}`)
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Suppression d'un article par un utilisateur non propriétaire", (done) => {
                chai.request(server)
                    .delete(`/api/articles/${firstAccount.articleId}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. Missing right');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Suppression d'un article par un administrateur", (done) => {
                chai.request(server)
                    .delete(`/api/articles/${thirdAccount.articleId}`)
                    .set({ Authorization: `Bearer ${secondAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'L\'article a été supprimé')
                        done();
                    })
            })
            test("Suppression d'un article par le propriétaire", (done) => {
                chai.request(server)
                    .delete(`/api/articles/${firstAccount.articleId}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'L\'article a été supprimé')
                        done();
                    })
            })
        })
    })
    suite('Commentaire', () => {
        suite('Création', () => {
            test("Création d'un commentaire sans authentification", (done) => {
                chai.request(server)
                    .post(`/api/comments/${firstAccount.articleId2}`)
                    .send({
                        "comment": 'Un nouveau commentaire'
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })

            test("Création d'un commentaire sans contenu texte", (done) => {
                chai.request(server)
                    .post(`/api/comments/${firstAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'comment');
                        assert.equal(res.body.errors[0].msg, 'Comment is required');
                        assert.equal(res.body.errors[0].location, 'body');
                        done();
                    })

            })
            test("Création d'un commentaire sans article associé", (done) => {
                chai.request(server)
                    .post(`/api/comments`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .send({
                        "comment": 'Un nouveau commentaire'
                    })
                    .end((err, res) => {
                        assert(res.body.type, "error");
                        assert(res.body.status, "400");
                        assert(res.body.message, "Validation failed");
                        done();
                    })
            })
            test("Création d'un commentaire sur un article inexistant", (done) => {
                chai.request(server)
                    .post(`/api/comments/0`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .send({
                        "comment": "Un nouveau commentaire"
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Article not found');
                        assert.equal(res.body.status, 404);
                        done();
                    })
            })
            test("Création d'un commentaire 1/5", (done) => {
                chai.request(server)
                    .post(`/api/comments/${firstAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .send({
                        "comment": "Un nouveau commentaire"
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Comment was created!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
            test("Création d'un commentaire 2/5", (done) => {
                chai.request(server)
                    .post(`/api/comments/${firstAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .send({
                        "comment": "Un second commentaire"
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Comment was created!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
            test("Création d'un commentaire 3/5", (done) => {
                chai.request(server)
                    .post(`/api/comments/${thirdAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .send({
                        "comment": "Un troisième commentaire"
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Comment was created!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
            test("Création d'un commentaire 4/5", (done) => {
                chai.request(server)
                    .post(`/api/comments/${thirdAccount.articleId3}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .send({
                        "comment": "Un quatrième commentaire"
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Comment was created!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
            test("Création d'un commentaire 5/5", (done) => {
                chai.request(server)
                    .post(`/api/comments/${thirdAccount.articleId3}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .send({
                        "comment": "Un cinquième commentaire"
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Comment was created!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
        })
        suite('Affichage', () => {
            test("Affichage des commentaires d'un article inexistant", (done) => {
                chai.request(server)
                    .get(`/api/comments/0`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.body.length, 0)
                        done();
                    })
            })
            test("Affichage des commentaires d'un article existant 1/2", (done) => {
                chai.request(server)
                    .get(`/api/comments/${firstAccount.articleId2}`)
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.body.length, 2)
                        thirdAccount.commentId = res.body[0].id;
                        thirdAccount.commentId2 = res.body[1].id;
                        done();
                    })
            })

            test("Affichage des commentaires d'un article existant 2/2", (done) => {
                chai.request(server)
                    .get(`/api/comments/${thirdAccount.articleId3}`)
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.body.length, 2)
                        firstAccount.commentId = res.body[0].id;
                        firstAccount.commentId2 = res.body[1].id;

                        done();
                    })
            })
            test("Affichage du nombre de commentaire d'un article existant", (done) => {
                chai.request(server)
                    .get(`/api/comments/count/${thirdAccount.articleId3}`)
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.body.countComment, 2)
                        done();
                    })
            })

        })
        suite('Modification', () => {
            test("Modification d'un commentaire sans utilisateur", (done) => {
                chai.request(server)
                    .put(`/api/comments/${thirdAccount.commentId}`)
                    .send({
                        "comment": "Un commentaire modifié"
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })

            test("Modification d'un commentaire sans contenu texte", (done) => {
                chai.request(server)
                    .put(`/api/comments/${thirdAccount.commentId}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Validation failed');
                        assert.equal(res.body.status, 400);
                        assert.equal(res.body.errors[0].param, 'content');
                        assert.equal(res.body.errors[0].msg, 'Comment is required');
                        assert.equal(res.body.errors[0].location, 'body');
                        done();
                    })
            })
            test("Modification d'un commentaire par un utilisateur non propriétaire", (done) => {
                chai.request(server)
                    .put(`/api/comments/${thirdAccount.commentId}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .send({
                        "content": "Un commentaire modifié"
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. Missing right');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Modification d'un commentaire par un administrateur", (done) => {
                chai.request(server)
                    .put(`/api/comments/${thirdAccount.commentId}`)
                    .set({ Authorization: `Bearer ${secondAccount.token}` })
                    .send({
                        "content": "Un commentaire modifié par un administrateur"
                    })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'Comment was updated!')
                        done();
                    })
            })
            test("Modification d'un commentaire par le propriétaire", (done) => {
                chai.request(server)
                    .put(`/api/comments/${thirdAccount.commentId}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .send({
                        "content": "Un commentaire modifié par le propriétaire du commentaire"
                    })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'Comment was updated!')
                        done();
                    })
            })
        })
        suite('Suppression', () => {
            test("Suppresion d'un commentaire par un utilisateur non propriétaire", (done) => {
                chai.request(server)
                    .delete(`/api/comments/${thirdAccount.commentId}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. Missing right');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Suppression d'un commentaire par un administrateur", (done) => {
                chai.request(server)
                    .delete(`/api/comments/${thirdAccount.commentId}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'Comment was deleted!')
                        done();
                    })
            })
            test("Suppression d'un commentaire par le propriétaire", (done) => {
                chai.request(server)
                    .delete(`/api/comments/${thirdAccount.commentId2}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'Comment was deleted!')
                        done();
                    })
            })
            test("Suppression d'un article supprime les commentaires associés", (done) => {
                chai.request(server)
                    .delete(`/api/articles/${thirdAccount.articleId3}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'L\'article a été supprimé')
                        chai.request(server)
                            .get(`/api/comments/${thirdAccount.articleId3}`)
                            .end((err, res) => {
                                assert.equal(res.status, 201)
                                done();
                            })
                    })
            })
        })
    })
    suite("like", () => {
        suite("Ajout", () => {
            test("Ajout d'un like sans authentification", (done) => {
                chai.request(server)
                    .post(`/api/likes/`)
                    .send({
                        article: firstAccount.articleId2
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Ajout d'un like sur un article inconnu", (done) => {
                chai.request(server)
                    .post(`/api/likes/`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .send({
                        articleid: '0'
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Article not found');
                        assert.equal(res.body.status, 404);
                        done();
                    })
            })
            test("Ajout d'un like", (done) => {
                chai.request(server)
                    .post(`/api/likes/`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .send({
                        articleid: thirdAccount.articleId2
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Like was added!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
            test("Ajout d'un like sur le même article, avec le même utilisateur", (done) => {
                chai.request(server)
                    .post(`/api/likes/`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .send({
                        articleid: thirdAccount.articleId2
                    })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.status, 409);
                        done()
                    })
            })
            test("Ajout d'un like sur le même article, avec un utilisateur différent", (done) => {
                chai.request(server)
                    .post(`/api/likes/`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .send({
                        articleid: thirdAccount.articleId2
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Like was added!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
            test("Ajout d'un like sur un autre article, avec le premier utilisateur", (done) => {
                chai.request(server)
                    .post(`/api/likes/`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .send({
                        articleid: firstAccount.articleId2
                    })
                    .end((err, res) => {
                        assert.equal(res.text, 'Like was added!')
                        assert.equal(res.status, 201);
                        done()
                    })
            })
        })
        suite("Affichage", () => {
            test("Affichage du score d'un article inexistant", (done) => {
                chai.request(server)
                    .get(`/api/likes/score/0`)
                    .end((err, res) => {
                        assert.equal(res.body.score, 0)
                        assert.equal(res.status, 201)
                        done()
                    })
            })

            test("Affichage du score d'un article avec like", (done) => {
                chai.request(server)
                    .get(`/api/likes/score/${thirdAccount.articleId2}`)
                    .end((err, res) => {
                        assert.equal(res.body.score, 2)
                        assert.equal(res.status, 201)
                        done()
                    })
            })
            test("affichage si l'utilisateur courant a mis un like sur un article", (done) => {
                chai.request(server)
                    .get(`/api/likes/${thirdAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.userlike, true)
                        assert.equal(res.status, 201)
                        done()
                    })
            })
            test("affichage si l'utilisateur courant n'a pas mis un like sur un article", (done) => {
                chai.request(server)
                    .get(`/api/likes/${firstAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.userlike, false)
                        assert.equal(res.status, 201)
                        done()
                    })
            })
        })
        suite("Suppression", () => {
            test("Supression d'un like sans authentification", (done) => {
                chai.request(server)
                    .delete(`/api/likes/${firstAccount.articleId2}`)
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Access denied. No credentials sent!');
                        assert.equal(res.body.status, 401);
                        done();
                    })
            })
            test("Suppression d'un like inexistant", (done) => {
                chai.request(server)
                    .delete(`/api/likes/${firstAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.body.type, 'error');
                        assert.equal(res.body.message, 'Like not found');
                        assert.equal(res.body.status, 404);
                        done();
                    })
            })
            test("Suppression d'un like d'un utilisateur", (done) => {
                chai.request(server)
                    .delete(`/api/likes/${thirdAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${firstAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.text, 'like was deleted!')
                        assert.equal(res.status, 201);
                        done();
                    })
            })
            test("Suppression d'un article supprime tous les like associés", (done) => {
                chai.request(server)
                    .delete(`/api/articles/${thirdAccount.articleId2}`)
                    .set({ Authorization: `Bearer ${thirdAccount.token}` })
                    .end((err, res) => {
                        assert.equal(res.status, 201);
                        assert.equal(res.text, 'L\'article a été supprimé')
                        chai.request(server)
                            .get(`/api/likes/score/${thirdAccount.articleId2}`)
                            .end((err, res) => {
                                assert.equal(res.body.score, 0)
                                assert.equal(res.status, 201)
                                done()
                            })
                    })
            })
        })
    })
    suite("uilisateurs créé dans la base de données :", () => {
        test(`Utilisateur 1 (role ${firstAccount.role}) : ${firstAccount.email} ; mot de passe : ${firstAccount.password}`, (done) => { done()})
        test(`Utilisateur 1 (role ${secondAccount.role}) : ${secondAccount.email} ; mot de passe : ${secondAccount.password}`, (done) => { done()});
        test(`Utilisateur 1 (role ${thirdAccount.role}) : ${thirdAccount.email} ; mot de passe : ${thirdAccount.password}`, (done) => { done()});
    }) 
})
