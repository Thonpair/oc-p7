const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createUserSchema, validateLogin } = require('../middleware/validators/userValidator.middleware');

router.post('/', createUserSchema, awaitHandlerFactory(userController.createUser)); // localhost:3000/api/users
router.post('/login', validateLogin, awaitHandlerFactory(userController.userLogin)); // localhost:3000/api/users/login
router.get('/whoami', auth(), awaitHandlerFactory(userController.infos));
router.get('/whois/:userid', awaitHandlerFactory(userController.infos));
module.exports = router;
