const express = require('express');
const router = express.Router();

const commentController = require('../controllers/comment.controller');

const auth = require('../middleware/auth.middleware');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createCommentSchema, updateCommentSchema } = require('../middleware/validators/commentValidator.middleware')

router.post('/:articleid', auth(), createCommentSchema, awaitHandlerFactory(commentController.createComment))
router.get('/:articleid', awaitHandlerFactory(commentController.getComments))
router.get('/count/:articleid', awaitHandlerFactory(commentController.getCountComments))
router.put('/:commentid', auth(), updateCommentSchema, awaitHandlerFactory(commentController.updateComment) )
router.delete('/:commentid', auth(), awaitHandlerFactory(commentController.deleteComment))
module.exports = router;