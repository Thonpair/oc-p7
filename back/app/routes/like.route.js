const express = require('express');
const router = express.Router();

const likeController = require('../controllers/like.controller');

const auth = require('../middleware/auth.middleware');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createLikeSchema } = require('../middleware/validators/likeValidator.middleware');

router.post('/', auth(), createLikeSchema, awaitHandlerFactory(likeController.createLike));
router.get('/score/:articleid', awaitHandlerFactory(likeController.countArticleLikes));
router.get('/:articleid', auth(), awaitHandlerFactory(likeController.userLike));
router.delete('/:articleid', auth(), createLikeSchema, awaitHandlerFactory(likeController.deleteLike));

module.exports = router;