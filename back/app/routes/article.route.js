const express = require('express');
const router = express.Router();

const articleController = require('../controllers/article.controller');

const auth = require('../middleware/auth.middleware');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');
const multer = require('../middleware/multer.middleware');

const { createArticleSchema, updateArticleSchema } = require('../middleware/validators/articleValidator.middleware');

router.post('/', auth(), multer, createArticleSchema, awaitHandlerFactory(articleController.createArticle))
router.get('/', awaitHandlerFactory(articleController.getArticles))
router.get('/:id', awaitHandlerFactory(articleController.getOneArticle))
router.put('/:id', auth(), multer, updateArticleSchema, awaitHandlerFactory(articleController.updateArticle))
router.delete('/:id', auth(), awaitHandlerFactory(articleController.deleteArticle))

module.exports = router;