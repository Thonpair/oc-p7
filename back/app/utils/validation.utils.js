const HttpException = require('../utils/HttpException.utils');
const { validationResult } = require('express-validator');

/** Analyse s'il y a eu des erreurs dans les paramètres */
exports.checkValidation = (req) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        throw new HttpException(400, 'Validation failed', errors);
    }
}

/** Analyse les paramètres et la présence de fichier */
exports.checkValidationAndFile = (req) => {
    const errors = validationResult(req)
    if(!req.file){
        errors.errors.push({value: undefined, msg: 'File is required', location: 'file'})
    }
    if (!errors.isEmpty()) {
        throw new HttpException(400, 'Validation failed', errors);
    }
}