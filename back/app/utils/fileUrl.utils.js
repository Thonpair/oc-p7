require('dotenv').config();
const path = require("path");
/** génère une url à partir d'un fichier */
exports.externalFilePath = (filename) => {    
    return process.env.URL + ':' + path.join(process.env.PORT,process.env.UPLOAD_URL_DIR, filename);
}
